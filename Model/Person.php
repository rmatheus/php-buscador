<?php
require_once ROOT_PATH."/Model/DatabaseModel.php";
include ROOT_PATH."Util/Constants.php";

class Person extends DatabaseModel
{
    public function getSearch($prop)
    {
        if(!is_null($prop)) {
            $prop = "%".$prop."%";
            return $this->select(Constants::SELECT_SEARCH_LIKE, ["s", $prop]);
        }
    }
}