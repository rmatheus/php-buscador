<?php
define("ROOT_PATH", __DIR__ . "/../");

// include main configuration file
require_once ROOT_PATH . "/Util/config.php";

// include the base controller file
require_once ROOT_PATH . "/Controller/BaseController.php";

// include the use model file
require_once ROOT_PATH . "/Model/Person.php";
?>