<?php

class Constants {
    const SELECT_SEARCH_LIKE = "SELECT * FROM persons INNER JOIN person_property AS pp ON persons.id = pp.id_person INNER JOIN properties ON pp.id_property = properties.id WHERE pp.detail LIKE ? ORDER BY persons.id ASC";
}