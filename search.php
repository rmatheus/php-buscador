<?php
require __DIR__ . "/Util/bootstrap.php";

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode( '/', $uri );

if ((isset($uri[3]) && $uri[3] != 'people') || !isset($uri[4])) {
    header("HTTP/1.1 404 Not Found");
    exit();
}

require ROOT_PATH . "/Controller/PersonController.php";

$objFeedController = new PersonController();
$strMethodName = $uri[4] . 'Action';
$objFeedController->{$strMethodName}();
?>