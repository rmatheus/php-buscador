<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap" rel="stylesheet">
    <title>Hello, world!</title>
    <style>
        @import url('http://fonts.cdnfonts.com/css/monaco');
        body {
            font-family: 'Roboto', sans-serif;
        }
        .abs-center {
            display: flex;
            align-items: center;
            justify-content: center;
            /* min-height: 50vh; */
        } 
    </style>
  </head>
  <body>
    <div class="container">     
        <div style="margin-top:10%">
            <div class="mb-3 abs-center">
                <span style="font-size: 60px; color:#f11f33">
                    I'm feeling lucky!
                </span> 
            </div>        
            
            <div class="mb-3 abs-center">
                <input id="data" type="text" class="form-control form-control-lg" >
            </div>
    
            <div class="mb-3 abs-center">
                <button type="button" id="toSearch" class="btn btn-primary" style="background-color:#f11f33">Search!</button>
            </div>
        </div>   
        <div class="section">
            <ul id="info"></ul>
        </div>
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script type="text/javascript">
        document.querySelector('#toSearch').addEventListener('click', function() {
            getData();
        });

        function getData() {
            let uri = 'http://localhost/pruebaphp/search.php/people/search?query=';
            let param = document.querySelector('#data').value;
            const api = new XMLHttpRequest();

            api.open('GET',uri+param,true);
            api.send();

            api.onreadystatechange = function() {
                if(this.status == 200 && this.readyState == 4) {
                    let datos = JSON.parse(this.responseText);
                    console.log(datos);
                    let info = document.querySelector('#info');
                    info.innerHTML = '';

                    for(let i of datos) {
                        console.log(i.detail);
                        info.innerHTML += `<li><b>${i.fullname}</b><ul><li>${i.name}</li><li>${i.detail}</li></ul>  </li> `
                    }
                }
            }
        }
    </script>
  </body>
</html>
