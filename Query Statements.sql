CREATE DATABASE pruebaphp;

use pruebaphp;
CREATE TABLE `persons` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `properties` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `person_property` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `detail` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `id_person` bigint(20) unsigned NOT NULL,
  `id_property` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (id_person) REFERENCES persons(id),
  FOREIGN KEY (id_property) REFERENCES properties(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO persons (fullname)
VALUES ('Juan'),
	   ('Irene'), 
       ('Manuel');

INSERT INTO properties (name)
VALUES ('color de los ojos'),
	   ('color del carro'), 
       ('color de la casa');
       
INSERT INTO person_property (detail,id_person,id_property)
VALUES ('azul claro', 1,1),
       ('azul claro',1,2), 
       ('azulados',2,1),
       ('azul',2,3),
       ('rojo',2,2),
       ('naranja',3,3);